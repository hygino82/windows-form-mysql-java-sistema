create database dbinfox;

use dbinfox;

create table tb_usuarios(
	iduser int primary key,
	usuario varchar(50) not null,
	fone varchar(15),
	login varchar(15) not null unique,
	senha varchar(15)not null
);

insert into tb_usuarios(iduser, usuario, fone, login, senha) 
values (1, 'José de Assis', '99988-1234', 'joseassis', '123abcd');

insert into tb_usuarios(iduser, usuario, fone, login, senha) 
values (2, 'Administrador', '99988-4841', 'admin', 'admin123abcd');

insert into tb_usuarios(iduser, usuario, fone, login, senha) 
values (3, 'Jupira', '99988-8946', 'jupira', 'juju123');

update tb_usuarios set fone='8888-33647' where iduser = 2;

delete from tb_usuarios where iduser = 3;

create table tb_cliente(
	idcli int primary key auto_increment,
	nomecli varchar(50) not null,
	endcli varchar(100),
	fonecli varchar(50) not null,
	emailcli varchar(50),
	cidadecli varchar(50)
);

insert into tb_cliente (nomecli, endcli, fonecli, emailcli, cidadecli)
values('Dilma Opressora','Rua das Antas 804','549987-4365','dilma@opressora.net','Porto Alegre');

create table tb_os(
	os int primary key auto_increment,
	data_os timestamp default  current_timestamp,
	equipamento varchar(150) not null,
	defeito varchar(150) not null,
	servico varchar(150),
	tecnico varchar(30),
	valor decimal(10, 2),
	idcli int not null,
	foreign key (idcli) references tb_cliente(idcli)
);

describe tb_os;

insert into tb_os(equipamento, defeito, servico, tecnico, valor, idcli)
values('CPU X79 Aterminter', 'Superaquecendo', 'Trocar cooler e pasta térmica', 'Juvenal', 87.45, 1);

select O.os, equipamento, defeito, servico, valor,
C.nomecli , fonecli
from tb_os as O
inner join tb_cliente as C
on (O.idcli = C.idcli); 

