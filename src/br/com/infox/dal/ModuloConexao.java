package br.com.infox.dal;

import java.sql.*;

public class ModuloConexao {

    public static Connection conector() {
        Connection conexao = null;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/dbinfox";
        String user = "developer";
        String password = "1234567";

        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, user, password);
            return conexao;
        } catch (SQLException e) {
            //System.out.println("Acesso negado!");
            return null;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
